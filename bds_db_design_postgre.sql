--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

-- Started on 2021-11-01 16:04:18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 858 (class 1247 OID 16411)
-- Name: omg; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.omg AS ENUM (
    'bronze',
    'silver',
    'gold'
);


ALTER TYPE public.omg OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 215 (class 1259 OID 17350)
-- Name: address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.address (
    address_id integer NOT NULL,
    city character varying(45) NOT NULL,
    street character varying(45) NOT NULL,
    house_number character varying(45) NOT NULL,
    zip_code character varying(45) NOT NULL
);


ALTER TABLE public.address OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 17349)
-- Name: address_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.address_address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_address_id_seq OWNER TO postgres;

--
-- TOC entry 3464 (class 0 OID 0)
-- Dependencies: 214
-- Name: address_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.address_address_id_seq OWNED BY public.address.address_id;


--
-- TOC entry 221 (class 1259 OID 17375)
-- Name: cinema; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cinema (
    cinema_id integer NOT NULL,
    cinema_name character varying(45) NOT NULL,
    address_id integer NOT NULL
);


ALTER TABLE public.cinema OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 17374)
-- Name: cinema_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cinema_address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cinema_address_id_seq OWNER TO postgres;

--
-- TOC entry 3465 (class 0 OID 0)
-- Dependencies: 220
-- Name: cinema_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cinema_address_id_seq OWNED BY public.cinema.address_id;


--
-- TOC entry 219 (class 1259 OID 17373)
-- Name: cinema_cinema_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cinema_cinema_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cinema_cinema_id_seq OWNER TO postgres;

--
-- TOC entry 3466 (class 0 OID 0)
-- Dependencies: 219
-- Name: cinema_cinema_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cinema_cinema_id_seq OWNED BY public.cinema.cinema_id;


--
-- TOC entry 226 (class 1259 OID 17402)
-- Name: genre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.genre (
    genre_id integer NOT NULL,
    genre_type character varying(45) NOT NULL
);


ALTER TABLE public.genre OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 17401)
-- Name: genre_genre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.genre_genre_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.genre_genre_id_seq OWNER TO postgres;

--
-- TOC entry 3467 (class 0 OID 0)
-- Dependencies: 225
-- Name: genre_genre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.genre_genre_id_seq OWNED BY public.genre.genre_id;


--
-- TOC entry 224 (class 1259 OID 17389)
-- Name: hall; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hall (
    hall_id integer NOT NULL,
    hall_name character varying(3) NOT NULL,
    seat_number integer NOT NULL,
    cinema_id integer NOT NULL
);


ALTER TABLE public.hall OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 17388)
-- Name: hall_cinema_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hall_cinema_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hall_cinema_id_seq OWNER TO postgres;

--
-- TOC entry 3468 (class 0 OID 0)
-- Dependencies: 223
-- Name: hall_cinema_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.hall_cinema_id_seq OWNED BY public.hall.cinema_id;


--
-- TOC entry 222 (class 1259 OID 17387)
-- Name: hall_hall_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hall_hall_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hall_hall_id_seq OWNER TO postgres;

--
-- TOC entry 3469 (class 0 OID 0)
-- Dependencies: 222
-- Name: hall_hall_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.hall_hall_id_seq OWNED BY public.hall.hall_id;


--
-- TOC entry 210 (class 1259 OID 17329)
-- Name: membership; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.membership (
    membership_id integer NOT NULL,
    membership_type character varying(45) NOT NULL,
    expiration date NOT NULL
);


ALTER TABLE public.membership OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 17328)
-- Name: membership_membership_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.membership_membership_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.membership_membership_id_seq OWNER TO postgres;

--
-- TOC entry 3470 (class 0 OID 0)
-- Dependencies: 209
-- Name: membership_membership_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.membership_membership_id_seq OWNED BY public.membership.membership_id;


--
-- TOC entry 229 (class 1259 OID 17410)
-- Name: movie; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movie (
    movie_id integer NOT NULL,
    title character varying(45) NOT NULL,
    duration integer NOT NULL,
    description character varying(45) NOT NULL,
    genre_id integer NOT NULL
);


ALTER TABLE public.movie OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 17409)
-- Name: movie_genre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movie_genre_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movie_genre_id_seq OWNER TO postgres;

--
-- TOC entry 3471 (class 0 OID 0)
-- Dependencies: 228
-- Name: movie_genre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movie_genre_id_seq OWNED BY public.movie.genre_id;


--
-- TOC entry 227 (class 1259 OID 17408)
-- Name: movie_movie_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movie_movie_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movie_movie_id_seq OWNER TO postgres;

--
-- TOC entry 3472 (class 0 OID 0)
-- Dependencies: 227
-- Name: movie_movie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movie_movie_id_seq OWNED BY public.movie.movie_id;


--
-- TOC entry 239 (class 1259 OID 17462)
-- Name: news; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.news (
    news_id integer NOT NULL,
    title character varying(45) NOT NULL,
    text character varying(45) NOT NULL,
    cinema_id integer NOT NULL
);


ALTER TABLE public.news OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 17461)
-- Name: news_cinema_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.news_cinema_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_cinema_id_seq OWNER TO postgres;

--
-- TOC entry 3473 (class 0 OID 0)
-- Dependencies: 238
-- Name: news_cinema_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.news_cinema_id_seq OWNED BY public.news.cinema_id;


--
-- TOC entry 237 (class 1259 OID 17460)
-- Name: news_news_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.news_news_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_news_id_seq OWNER TO postgres;

--
-- TOC entry 3474 (class 0 OID 0)
-- Dependencies: 237
-- Name: news_news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.news_news_id_seq OWNED BY public.news.news_id;


--
-- TOC entry 242 (class 1259 OID 17476)
-- Name: seat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seat (
    seat_id integer NOT NULL,
    availability boolean NOT NULL,
    show_id integer NOT NULL
);


ALTER TABLE public.seat OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 17474)
-- Name: seat_seat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seat_seat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seat_seat_id_seq OWNER TO postgres;

--
-- TOC entry 3475 (class 0 OID 0)
-- Dependencies: 240
-- Name: seat_seat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seat_seat_id_seq OWNED BY public.seat.seat_id;


--
-- TOC entry 241 (class 1259 OID 17475)
-- Name: seat_show_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seat_show_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seat_show_id_seq OWNER TO postgres;

--
-- TOC entry 3476 (class 0 OID 0)
-- Dependencies: 241
-- Name: seat_show_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seat_show_id_seq OWNED BY public.seat.show_id;


--
-- TOC entry 233 (class 1259 OID 17425)
-- Name: show; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.show (
    show_id integer NOT NULL,
    show_start date NOT NULL,
    show_end date NOT NULL,
    price integer NOT NULL,
    movie_id integer NOT NULL,
    hall_id integer NOT NULL
);


ALTER TABLE public.show OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 17424)
-- Name: show_hall_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.show_hall_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.show_hall_id_seq OWNER TO postgres;

--
-- TOC entry 3477 (class 0 OID 0)
-- Dependencies: 232
-- Name: show_hall_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.show_hall_id_seq OWNED BY public.show.hall_id;


--
-- TOC entry 231 (class 1259 OID 17423)
-- Name: show_movie_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.show_movie_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.show_movie_id_seq OWNER TO postgres;

--
-- TOC entry 3478 (class 0 OID 0)
-- Dependencies: 231
-- Name: show_movie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.show_movie_id_seq OWNED BY public.show.movie_id;


--
-- TOC entry 230 (class 1259 OID 17422)
-- Name: show_show_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.show_show_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.show_show_id_seq OWNER TO postgres;

--
-- TOC entry 3479 (class 0 OID 0)
-- Dependencies: 230
-- Name: show_show_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.show_show_id_seq OWNED BY public.show.show_id;


--
-- TOC entry 213 (class 1259 OID 17337)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    user_id integer NOT NULL,
    given_name character varying(45) NOT NULL,
    family_name character varying(45) NOT NULL,
    mail character varying(45) NOT NULL,
    phone_number integer,
    membership_id integer NOT NULL,
    password character varying(45) NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 17358)
-- Name: user_has_address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_has_address (
    user_id integer NOT NULL,
    address_id integer NOT NULL,
    address_type character varying(45) NOT NULL
);


ALTER TABLE public.user_has_address OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 17357)
-- Name: user_has_address_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_has_address_address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_has_address_address_id_seq OWNER TO postgres;

--
-- TOC entry 3480 (class 0 OID 0)
-- Dependencies: 217
-- Name: user_has_address_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_has_address_address_id_seq OWNED BY public.user_has_address.address_id;


--
-- TOC entry 216 (class 1259 OID 17356)
-- Name: user_has_address_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_has_address_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_has_address_user_id_seq OWNER TO postgres;

--
-- TOC entry 3481 (class 0 OID 0)
-- Dependencies: 216
-- Name: user_has_address_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_has_address_user_id_seq OWNED BY public.user_has_address.user_id;


--
-- TOC entry 236 (class 1259 OID 17445)
-- Name: user_has_show; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_has_show (
    user_id integer NOT NULL,
    show_id integer NOT NULL
);


ALTER TABLE public.user_has_show OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 17444)
-- Name: user_has_show_show_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_has_show_show_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_has_show_show_id_seq OWNER TO postgres;

--
-- TOC entry 3482 (class 0 OID 0)
-- Dependencies: 235
-- Name: user_has_show_show_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_has_show_show_id_seq OWNED BY public.user_has_show.show_id;


--
-- TOC entry 234 (class 1259 OID 17443)
-- Name: user_has_show_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_has_show_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_has_show_user_id_seq OWNER TO postgres;

--
-- TOC entry 3483 (class 0 OID 0)
-- Dependencies: 234
-- Name: user_has_show_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_has_show_user_id_seq OWNED BY public.user_has_show.user_id;


--
-- TOC entry 212 (class 1259 OID 17336)
-- Name: user_membership_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_membership_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_membership_id_seq OWNER TO postgres;

--
-- TOC entry 3484 (class 0 OID 0)
-- Dependencies: 212
-- Name: user_membership_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_membership_id_seq OWNED BY public."user".membership_id;


--
-- TOC entry 211 (class 1259 OID 17335)
-- Name: user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_user_id_seq OWNER TO postgres;

--
-- TOC entry 3485 (class 0 OID 0)
-- Dependencies: 211
-- Name: user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_user_id_seq OWNED BY public."user".user_id;


--
-- TOC entry 3235 (class 2604 OID 17353)
-- Name: address address_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address ALTER COLUMN address_id SET DEFAULT nextval('public.address_address_id_seq'::regclass);


--
-- TOC entry 3238 (class 2604 OID 17378)
-- Name: cinema cinema_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cinema ALTER COLUMN cinema_id SET DEFAULT nextval('public.cinema_cinema_id_seq'::regclass);


--
-- TOC entry 3239 (class 2604 OID 17379)
-- Name: cinema address_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cinema ALTER COLUMN address_id SET DEFAULT nextval('public.cinema_address_id_seq'::regclass);


--
-- TOC entry 3242 (class 2604 OID 17405)
-- Name: genre genre_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genre ALTER COLUMN genre_id SET DEFAULT nextval('public.genre_genre_id_seq'::regclass);


--
-- TOC entry 3240 (class 2604 OID 17392)
-- Name: hall hall_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hall ALTER COLUMN hall_id SET DEFAULT nextval('public.hall_hall_id_seq'::regclass);


--
-- TOC entry 3241 (class 2604 OID 17393)
-- Name: hall cinema_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hall ALTER COLUMN cinema_id SET DEFAULT nextval('public.hall_cinema_id_seq'::regclass);


--
-- TOC entry 3232 (class 2604 OID 17332)
-- Name: membership membership_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership ALTER COLUMN membership_id SET DEFAULT nextval('public.membership_membership_id_seq'::regclass);


--
-- TOC entry 3243 (class 2604 OID 17413)
-- Name: movie movie_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movie ALTER COLUMN movie_id SET DEFAULT nextval('public.movie_movie_id_seq'::regclass);


--
-- TOC entry 3244 (class 2604 OID 17414)
-- Name: movie genre_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movie ALTER COLUMN genre_id SET DEFAULT nextval('public.movie_genre_id_seq'::regclass);


--
-- TOC entry 3250 (class 2604 OID 17465)
-- Name: news news_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news ALTER COLUMN news_id SET DEFAULT nextval('public.news_news_id_seq'::regclass);


--
-- TOC entry 3251 (class 2604 OID 17466)
-- Name: news cinema_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news ALTER COLUMN cinema_id SET DEFAULT nextval('public.news_cinema_id_seq'::regclass);


--
-- TOC entry 3252 (class 2604 OID 17479)
-- Name: seat seat_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seat ALTER COLUMN seat_id SET DEFAULT nextval('public.seat_seat_id_seq'::regclass);


--
-- TOC entry 3253 (class 2604 OID 17480)
-- Name: seat show_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seat ALTER COLUMN show_id SET DEFAULT nextval('public.seat_show_id_seq'::regclass);


--
-- TOC entry 3245 (class 2604 OID 17428)
-- Name: show show_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.show ALTER COLUMN show_id SET DEFAULT nextval('public.show_show_id_seq'::regclass);


--
-- TOC entry 3246 (class 2604 OID 17429)
-- Name: show movie_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.show ALTER COLUMN movie_id SET DEFAULT nextval('public.show_movie_id_seq'::regclass);


--
-- TOC entry 3247 (class 2604 OID 17430)
-- Name: show hall_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.show ALTER COLUMN hall_id SET DEFAULT nextval('public.show_hall_id_seq'::regclass);


--
-- TOC entry 3233 (class 2604 OID 17340)
-- Name: user user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN user_id SET DEFAULT nextval('public.user_user_id_seq'::regclass);


--
-- TOC entry 3234 (class 2604 OID 17341)
-- Name: user membership_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN membership_id SET DEFAULT nextval('public.user_membership_id_seq'::regclass);


--
-- TOC entry 3236 (class 2604 OID 17361)
-- Name: user_has_address user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_has_address ALTER COLUMN user_id SET DEFAULT nextval('public.user_has_address_user_id_seq'::regclass);


--
-- TOC entry 3237 (class 2604 OID 17362)
-- Name: user_has_address address_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_has_address ALTER COLUMN address_id SET DEFAULT nextval('public.user_has_address_address_id_seq'::regclass);


--
-- TOC entry 3248 (class 2604 OID 17448)
-- Name: user_has_show user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_has_show ALTER COLUMN user_id SET DEFAULT nextval('public.user_has_show_user_id_seq'::regclass);


--
-- TOC entry 3249 (class 2604 OID 17449)
-- Name: user_has_show show_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_has_show ALTER COLUMN show_id SET DEFAULT nextval('public.user_has_show_show_id_seq'::regclass);


--
-- TOC entry 3431 (class 0 OID 17350)
-- Dependencies: 215
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.address (address_id, city, street, house_number, zip_code) FROM stdin;
1	Brno	Koniklecová	1	63400
2	Brno	Slezákova	6	61300
3	Brno	Netroufalky	10	62500
4	Brno	Anenská	34	60200
5	Brno	Antonínská	8	60200
\.


--
-- TOC entry 3437 (class 0 OID 17375)
-- Dependencies: 221
-- Data for Name: cinema; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cinema (cinema_id, cinema_name, address_id) FROM stdin;
1	Scala	1
2	Maxim	2
3	Olympia	3
4	Špalíček	4
5	Art	5
\.


--
-- TOC entry 3442 (class 0 OID 17402)
-- Dependencies: 226
-- Data for Name: genre; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.genre (genre_id, genre_type) FROM stdin;
1	komedie
2	muzikál
3	bajka
4	povídka
5	sonet
\.


--
-- TOC entry 3440 (class 0 OID 17389)
-- Dependencies: 224
-- Data for Name: hall; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.hall (hall_id, hall_name, seat_number, cinema_id) FROM stdin;
1	A	21	1
2	B	15	2
3	C	4	3
4	D	8	4
5	E	10	5
\.


--
-- TOC entry 3426 (class 0 OID 17329)
-- Dependencies: 210
-- Data for Name: membership; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.membership (membership_id, membership_type, expiration) FROM stdin;
1	bronze	2021-12-03
2	gold	2021-11-20
3	silver	2021-10-14
4	bronze	2021-05-03
5	bronze	2021-08-09
6	gold	2021-05-02
7	silver	2021-04-01
8	gold	2021-10-02
9	bronze	2021-02-02
10	bronze	2021-05-15
11	silver	2021-10-02
12	silver	2021-12-02
13	gold	2021-05-02
14	silver	2021-07-08
15	bronze	2021-05-24
16	silver	2021-05-15
17	gold	2021-05-13
18	silver	2021-04-19
19	bronze	2021-05-29
20	silver	2021-07-21
21	gold	2021-05-12
22	bronze	2021-05-13
23	silver	2021-05-17
24	bronze	2021-05-19
25	bronze	2021-05-18
26	silver	2021-05-25
27	gold	2021-05-19
28	bronze	2021-05-14
29	silver	2021-05-10
30	bronze	2021-05-04
31	silver	2021-10-02
32	bronze	2021-09-02
33	silver	2021-03-02
34	gold	2021-10-02
35	silver	2021-11-02
36	bronze	2021-10-02
37	silver	2021-12-02
38	gold	2021-09-02
39	silver	2021-06-02
40	bronze	2021-04-02
41	silver	2021-03-02
42	bronze	2021-08-02
43	bronze	2021-03-02
44	silver	2021-05-02
45	gold	2021-08-02
46	bronze	2021-03-02
47	silver	2021-01-02
48	gold	2021-05-02
49	silver	2021-04-02
50	silver	2021-08-02
\.


--
-- TOC entry 3445 (class 0 OID 17410)
-- Dependencies: 229
-- Data for Name: movie; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.movie (movie_id, title, duration, description, genre_id) FROM stdin;
1	Karel	127	film o Karlu Gottovi	1
2	Venom 2	90	film na motivy komisků	2
3	Zpráva	94	válečný film	3
4	Zhoubné zlo	101	o ženě, která má strach ze života	4
5	Matky	95	česká komedie	5
\.


--
-- TOC entry 3455 (class 0 OID 17462)
-- Dependencies: 239
-- Data for Name: news; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.news (news_id, title, text, cinema_id) FROM stdin;
1	Avatar 2	připravovaný sci-fi	1
2	Mimoni	připravovaná komedie	2
3	Morbius	připravovaný fantasy	3
4	Top Gun 2	připravovaný akční	4
5	Jan Žižka	připravovaný nejdražší film	5
\.


--
-- TOC entry 3458 (class 0 OID 17476)
-- Dependencies: 242
-- Data for Name: seat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.seat (seat_id, availability, show_id) FROM stdin;
1	t	1
2	f	2
3	t	3
4	f	4
5	f	5
\.


--
-- TOC entry 3449 (class 0 OID 17425)
-- Dependencies: 233
-- Data for Name: show; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.show (show_id, show_start, show_end, price, movie_id, hall_id) FROM stdin;
1	2020-02-05	2020-02-05	200	1	1
2	2021-04-11	2021-04-11	125	2	2
3	2021-06-20	2021-06-20	225	3	3
4	2021-09-01	2021-09-01	99	4	4
5	2021-12-25	2021-12-25	150	5	5
\.


--
-- TOC entry 3429 (class 0 OID 17337)
-- Dependencies: 213
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (user_id, given_name, family_name, mail, phone_number, membership_id, password) FROM stdin;
1	Roman	Novák	novak@ecd.com	\N	1	nazdar321
2	Adam	Dolejš	doli@seznam.cz	\N	2	pavel15
4	Filip	Kozák	koz@seznam.cz	\N	3	holahola5
3	Honza	Adamský	ada@seznam.cz	\N	4	.*+22ad+
5	Vladimír	Filip	vlafi@gmail.com	\N	5	x532ch
6	Filip	Horák	horak@seznam.cz	\N	6	#cKKYA8jk(=#\\dJk
7	Adam	Buran	buran@seznam.cz	\N	7	&V,63RB\\:TsjA7CU
8	Honza	Machát	machat@seznam.cz	\N	8	(+=7kxE]hfR~9ET*
9	Libor	Kulka	kulka@seznam.cz	\N	9	5SQm9%WJAeZQ%`>L
10	Hugo	Kaňa	kana@seznam.cz	\N	10	C7[)[vP\\a~7YsBMD
11	Vendelín	Mlčák	mlcak@seznam.cz	\N	11	~-"6}M,(N-scfJdt
12	Milan	Žilka	zilka@seznam.cz	\N	12	dS#WyK<LRXH6Dx.\\
13	Čestmír	Folprecht	folprecht@seznam.cz	\N	13	y-6V{Bnn@#]Eu)nH
14	Zikmund	Rafaj	rafaj@seznam.cz	\N	14	qLv!M3~u(R/Ky~"~
15	Lubor	Wojnar	wojnar@seznam.cz	\N	15	f}TE__,UDzWMvb8A
16	Bořek	Klapka	klapka@seznam.cz	\N	16	w3%z%]/Jx@m{<Mr?
17	Vendelín	Kraft	kraft@seznam.cz	\N	17	"x}*$M:m)\\)w:L3
18	Lumír	Šigut	sigut@seznam.cz	\N	18	m]"Hhx5a?j#"q5&a
19	Boleslav	Hajda	hajda@seznam.cz	\N	19	~[eZ5u~TRJ8rwD,(
20	Štěpán	Poslušný	poslusny@seznam.cz	\N	20	$SQs--#%Kr4x^Exv
21	Matěj	Valter	valter@seznam.cz	\N	21	Lh8_B*!#w6mX6Wvn
22	Kazimír	Hladík	hladik@seznam.cz	\N	22	Xfb#Y=fn+-Y6u!X8
23	Vratislav	Lang	lang@seznam.cz	\N	23	qSk2Q@M+w&yW$h9Y
24	Petr	Vytlačil	vylacil@seznam.cz	\N	24	!L9NdtMm5q!eAuHQ
25	Gabriel	Dolanský	dolansky@seznam.cz	\N	25	+HSdd9K$3+t#5fsm
26	Vít	Lukášek	lukasek@seznam.cz	\N	26	jG89bnX_!kcPj!XY
27	Radovan	Švejda	svejda@seznam.cz	\N	27	7NUTs$=D^#Tbf8a3
28	Ingrid	Gebauer	gebauer@seznam.cz	\N	28	Y3jqzx6^w%vNxk4n
29	Věroslav	Steiner	steiner@seznam.cz	\N	29	yQS?q^4$HuUqc29C
30	Mojmír	Švancara	svancara@seznam.cz	\N	30	ZH3B#u^dvw@gHavY
31	Josef	Čajka	cajka@seznam.cz	\N	31	%m?e3?m^_3XXT+QS
32	Vojtěch	Rýdl	rydl@seznam.cz	\N	32	CzTh#J&3WsbQP?Zm
33	Metoděj	Šafránek	safranek@seznam.cz	\N	33	%3=&Zm@6$hfXG!=7
34	Karel	Hák	hak@seznam.cz	\N	34	%3=&Zm@6$hfXG!=7
35	Vojtěch	Pátek	patk@seznam.cz	\N	35	!pbF-Rm$=KHqb6Lw
36	Pankrác	Zabloudil	zabloudil@seznam.cz	\N	36	-8RkPF*$b8j*5LVq
37	Rút	Valášková	valaskova@seznam.cz	\N	37	h2-jMc&TTkmZ6!XE
38	Evelína	Jehličková	jehlickova@seznam.cz	\N	38	#rvA5qXhLjETZcV&
39	Karina	Plívová	plivova@seznam.cz	\N	39	S2urXkc%Pmt26E+A
40	Radka	Vlčková	vlckova@seznam.cz	\N	40	=eT+Bngcm29KNYKE
41	Ema	Hanáčková	hanackova@seznam.cz	\N	41	kbN@%gQD4uwbc^T#
42	Kamila	Čiperová	ciperova@seznam.cz	\N	42	*xF^+k8wEcpxeQja
43	Regína	Filipová	filipova@seznam.cz	\N	43	Sw+CSWe!Rdqj*Q4h
44	Alžběta	Borůvková	boruvkova@seznam.cz	\N	44	Syu@ajJC?94+HeWD
45	Marcela	Jůnová	junova@seznam.cz	\N	45	X?9*83qs5tPtz8%V
46	Natálie	Kazdová	kazdova@seznam.cz	\N	46	vs&NMh6BXWHGs_-U
47	Amálie	Bártů	bartu@seznam.cz	\N	47	-bBjYKVeu*D2Gw%V
48	Martina	Drlíková	drlikova@seznam.cz	\N	48	FDVrzC!e5JNsa+6!
49	Vilma	Nosková	noskova@seznam.cz	\N	49	3W_fA?zQk&R5-C$Y
50	Andrea	Stará	stara@seznam.cz	\N	50	qF4C@z$?ph&9fk!R
\.


--
-- TOC entry 3434 (class 0 OID 17358)
-- Dependencies: 218
-- Data for Name: user_has_address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_has_address (user_id, address_id, address_type) FROM stdin;
1	1	byt
2	2	byt
3	3	byt
4	4	byt
5	5	byt
\.


--
-- TOC entry 3452 (class 0 OID 17445)
-- Dependencies: 236
-- Data for Name: user_has_show; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_has_show (user_id, show_id) FROM stdin;
1	1
2	2
3	3
4	4
5	5
\.


--
-- TOC entry 3486 (class 0 OID 0)
-- Dependencies: 214
-- Name: address_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.address_address_id_seq', 5, true);


--
-- TOC entry 3487 (class 0 OID 0)
-- Dependencies: 220
-- Name: cinema_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cinema_address_id_seq', 1, false);


--
-- TOC entry 3488 (class 0 OID 0)
-- Dependencies: 219
-- Name: cinema_cinema_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cinema_cinema_id_seq', 5, true);


--
-- TOC entry 3489 (class 0 OID 0)
-- Dependencies: 225
-- Name: genre_genre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.genre_genre_id_seq', 5, true);


--
-- TOC entry 3490 (class 0 OID 0)
-- Dependencies: 223
-- Name: hall_cinema_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hall_cinema_id_seq', 1, false);


--
-- TOC entry 3491 (class 0 OID 0)
-- Dependencies: 222
-- Name: hall_hall_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hall_hall_id_seq', 5, true);


--
-- TOC entry 3492 (class 0 OID 0)
-- Dependencies: 209
-- Name: membership_membership_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.membership_membership_id_seq', 50, true);


--
-- TOC entry 3493 (class 0 OID 0)
-- Dependencies: 228
-- Name: movie_genre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movie_genre_id_seq', 1, false);


--
-- TOC entry 3494 (class 0 OID 0)
-- Dependencies: 227
-- Name: movie_movie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movie_movie_id_seq', 5, true);


--
-- TOC entry 3495 (class 0 OID 0)
-- Dependencies: 238
-- Name: news_cinema_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.news_cinema_id_seq', 1, false);


--
-- TOC entry 3496 (class 0 OID 0)
-- Dependencies: 237
-- Name: news_news_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.news_news_id_seq', 5, true);


--
-- TOC entry 3497 (class 0 OID 0)
-- Dependencies: 240
-- Name: seat_seat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seat_seat_id_seq', 5, true);


--
-- TOC entry 3498 (class 0 OID 0)
-- Dependencies: 241
-- Name: seat_show_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seat_show_id_seq', 1, false);


--
-- TOC entry 3499 (class 0 OID 0)
-- Dependencies: 232
-- Name: show_hall_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.show_hall_id_seq', 1, false);


--
-- TOC entry 3500 (class 0 OID 0)
-- Dependencies: 231
-- Name: show_movie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.show_movie_id_seq', 1, false);


--
-- TOC entry 3501 (class 0 OID 0)
-- Dependencies: 230
-- Name: show_show_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.show_show_id_seq', 5, true);


--
-- TOC entry 3502 (class 0 OID 0)
-- Dependencies: 217
-- Name: user_has_address_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_has_address_address_id_seq', 1, false);


--
-- TOC entry 3503 (class 0 OID 0)
-- Dependencies: 216
-- Name: user_has_address_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_has_address_user_id_seq', 1, false);


--
-- TOC entry 3504 (class 0 OID 0)
-- Dependencies: 235
-- Name: user_has_show_show_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_has_show_show_id_seq', 1, false);


--
-- TOC entry 3505 (class 0 OID 0)
-- Dependencies: 234
-- Name: user_has_show_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_has_show_user_id_seq', 1, false);


--
-- TOC entry 3506 (class 0 OID 0)
-- Dependencies: 212
-- Name: user_membership_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_membership_id_seq', 1, false);


--
-- TOC entry 3507 (class 0 OID 0)
-- Dependencies: 211
-- Name: user_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_user_id_seq', 50, true);


--
-- TOC entry 3259 (class 2606 OID 17355)
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (address_id);


--
-- TOC entry 3261 (class 2606 OID 17381)
-- Name: cinema cinema_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cinema
    ADD CONSTRAINT cinema_pkey PRIMARY KEY (cinema_id);


--
-- TOC entry 3265 (class 2606 OID 17407)
-- Name: genre genre_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genre
    ADD CONSTRAINT genre_pkey PRIMARY KEY (genre_id);


--
-- TOC entry 3263 (class 2606 OID 17395)
-- Name: hall hall_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hall
    ADD CONSTRAINT hall_pkey PRIMARY KEY (hall_id);


--
-- TOC entry 3255 (class 2606 OID 17334)
-- Name: membership membership_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership
    ADD CONSTRAINT membership_pkey PRIMARY KEY (membership_id);


--
-- TOC entry 3267 (class 2606 OID 17416)
-- Name: movie movie_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movie
    ADD CONSTRAINT movie_pkey PRIMARY KEY (movie_id);


--
-- TOC entry 3271 (class 2606 OID 17468)
-- Name: news news_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news
    ADD CONSTRAINT news_pkey PRIMARY KEY (news_id);


--
-- TOC entry 3273 (class 2606 OID 17482)
-- Name: seat seat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seat
    ADD CONSTRAINT seat_pkey PRIMARY KEY (seat_id);


--
-- TOC entry 3269 (class 2606 OID 17432)
-- Name: show show_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.show
    ADD CONSTRAINT show_pkey PRIMARY KEY (show_id);


--
-- TOC entry 3257 (class 2606 OID 17343)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (user_id);


--
-- TOC entry 3277 (class 2606 OID 17382)
-- Name: cinema cinema_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cinema
    ADD CONSTRAINT cinema_address_id_fkey FOREIGN KEY (address_id) REFERENCES public.address(address_id);


--
-- TOC entry 3278 (class 2606 OID 17396)
-- Name: hall hall_cinema_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hall
    ADD CONSTRAINT hall_cinema_id_fkey FOREIGN KEY (cinema_id) REFERENCES public.cinema(cinema_id);


--
-- TOC entry 3279 (class 2606 OID 17417)
-- Name: movie movie_genre_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movie
    ADD CONSTRAINT movie_genre_id_fkey FOREIGN KEY (genre_id) REFERENCES public.genre(genre_id);


--
-- TOC entry 3284 (class 2606 OID 17469)
-- Name: news news_cinema_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news
    ADD CONSTRAINT news_cinema_id_fkey FOREIGN KEY (cinema_id) REFERENCES public.cinema(cinema_id);


--
-- TOC entry 3285 (class 2606 OID 17483)
-- Name: seat seat_show_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seat
    ADD CONSTRAINT seat_show_id_fkey FOREIGN KEY (show_id) REFERENCES public.show(show_id);


--
-- TOC entry 3281 (class 2606 OID 17438)
-- Name: show show_hall_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.show
    ADD CONSTRAINT show_hall_id_fkey FOREIGN KEY (hall_id) REFERENCES public.hall(hall_id);


--
-- TOC entry 3280 (class 2606 OID 17433)
-- Name: show show_movie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.show
    ADD CONSTRAINT show_movie_id_fkey FOREIGN KEY (movie_id) REFERENCES public.movie(movie_id);


--
-- TOC entry 3276 (class 2606 OID 17368)
-- Name: user_has_address user_has_address_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_has_address
    ADD CONSTRAINT user_has_address_address_id_fkey FOREIGN KEY (address_id) REFERENCES public.address(address_id);


--
-- TOC entry 3275 (class 2606 OID 17363)
-- Name: user_has_address user_has_address_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_has_address
    ADD CONSTRAINT user_has_address_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(user_id);


--
-- TOC entry 3283 (class 2606 OID 17455)
-- Name: user_has_show user_has_show_show_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_has_show
    ADD CONSTRAINT user_has_show_show_id_fkey FOREIGN KEY (show_id) REFERENCES public.show(show_id);


--
-- TOC entry 3282 (class 2606 OID 17450)
-- Name: user_has_show user_has_show_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_has_show
    ADD CONSTRAINT user_has_show_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(user_id);


--
-- TOC entry 3274 (class 2606 OID 17344)
-- Name: user user_membership_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_membership_id_fkey FOREIGN KEY (membership_id) REFERENCES public.membership(membership_id);


-- Completed on 2021-11-01 16:04:19

--
-- PostgreSQL database dump complete
--

